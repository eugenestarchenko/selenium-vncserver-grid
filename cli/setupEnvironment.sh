#!/bin/bash

rm -rf ~/.vnc/
mkdir ~/.vnc/

echo "SUAV+aNeiyI=" | base64 -d > ~/.vnc/passwd
chmod 0600  ~/.vnc/passwd

tee ~/.vnc/xstartup <<EOF
!/bin/sh

xrdb \$HOME/.Xresources
#xsetroot -solid grey
#export XKL_XMODMAP_DISABLE=1
#startlxde &

java -jar /projects/selenium-hubber/bin/selenium-server-standalone-2.33.0.jar -role node  -hub http://selenium-hub.suho.local:4444/grid/register

EOF

chmod +x ~/.vnc/xstartup

vncserver :1
