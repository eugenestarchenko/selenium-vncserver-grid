#!/bin/bash

SELENIUM_ROOT_DIR="$( cd "$( dirname "$0" )"/../ && pwd )"

echo -n "Setting up vnc:"

echo -n " dir"
rm -rf ~/.vnc/ || exit 1
mkdir ~/.vnc/ || exit 1

# The password for 123456

echo -n " password"
echo "SUAV+aNeiyI=" | base64 -d | tee -a ~/.vnc/passwd > /dev/null || exit 1
chmod 0600  ~/.vnc/passwd  || exit 1
tee ~/.vnc/xstartup > /dev/null <<EOF
#!/bin/sh

xrdb \$HOME/.Xresources
#xsetroot -solid grey
#export XKL_XMODMAP_DISABLE=1
#startlxde &

ratpoison &

java -jar $SELENIUM_ROOT_DIR/bin/selenium-server-standalone-2.33.0.jar -role node -port $NODE_PORT -hub http://localhost:4444/grid/register

EOF

chmod +x ~/.vnc/xstartup

echo -n " start"
vncserver $DISPLAY_TO_START -geometry 1280x968
