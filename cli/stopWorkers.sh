#!/bin/bash
SELENIUM_ROOT_DIR="$( cd "$( dirname "$0" )"/../ && pwd )"

WORKERS_TO_START=${WORKERS_TO_START:=4}

for I in `seq 1 $WORKERS_TO_START`; do 
    export NODE_PORT=$((5555 - 1 + $I))
    export DISPLAY_TO_STOP=:$((5921 - 1 + $I))
    export THE_USERNAME=selwork$I
    su $THE_USERNAME -c $SELENIUM_ROOT_DIR/cli/terminateWorker.sh || exit 1
done
