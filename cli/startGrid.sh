#!/bin/bash

SELENIUM_ROOT_DIR="$( cd "$( dirname "$0" )"/../ && pwd )"

WORKERS_TO_START=${WORKERS_TO_START:=4}

echo $WORKERS_TO_START

exit

NODE_PORT=5555 DISPLAY_TO_START=:5921 su seleniumworker1 -c $SELENIUM_ROOT_DIR/cli/startWorker.sh
NODE_PORT=5556 DISPLAY_TO_START=:5922 su seleniumworker2 -c $SELENIUM_ROOT_DIR/cli/startWorker.sh
NODE_PORT=5557 DISPLAY_TO_START=:5923 su seleniumworker3 -c $SELENIUM_ROOT_DIR/cli/startWorker.sh
NODE_PORT=5558 DISPLAY_TO_START=:5924 su seleniumworker4 -c $SELENIUM_ROOT_DIR/cli/startWorker.sh
